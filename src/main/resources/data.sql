
INSERT INTO FOOD_CATEGORY (id, name, cli_name) VALUES (1, 'Drink', 'drink');
INSERT INTO FOOD_CATEGORY (id, name, cli_name) VALUES (2, 'Chocolate', 'chocolate');
INSERT INTO FOOD_CATEGORY (id, name, cli_name) VALUES (3, 'Other', 'other');


INSERT INTO FOOD_ITEM (id, name, price, quantity, food_category_id, cli_name) VALUES (1, 'Coke', 1.20, 3, 1, 'coke');
INSERT INTO FOOD_ITEM (id, name, price, quantity, food_category_id, cli_name) VALUES (2, 'Diet Coke', 1.10, 5, 1, 'diet-coke');
INSERT INTO FOOD_ITEM (id, name, price, quantity, food_category_id, cli_name) VALUES (3, 'Vittel', 1.20, 4, 1, 'vittel');
INSERT INTO FOOD_ITEM (id, name, price, quantity, food_category_id, cli_name) VALUES (4, 'Snickers', 0.80, 3, 2, 'snickers');
INSERT INTO FOOD_ITEM (id, name, price, quantity, food_category_id, cli_name) VALUES (5, 'Mars', 0.70, 15, 2, 'mars');
INSERT INTO FOOD_ITEM (id, name, price, quantity, food_category_id, cli_name) VALUES (6, 'Chewing-gum', 1.00, 3, 3, 'chewing-gum');