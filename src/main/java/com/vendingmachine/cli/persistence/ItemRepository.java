package com.vendingmachine.cli.persistence;

import com.vendingmachine.cli.model.FoodItem;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ItemRepository extends JpaRepository<FoodItem, Long> {

    /**
     * Find Vending machine items by their cli-name
     * @param id
     * @return
     */
    @Query("FROM FoodItem i where i.cliName = :id")
    Optional<FoodItem> findByCliId(@Param("id") String id);

    //@Query("SELECT i.name FROM FoodItem i where i.category.id = :id and i.quantity > 0")
    //List<FoodItem> findByCategory(@Param("category") Long categoryId);
}
