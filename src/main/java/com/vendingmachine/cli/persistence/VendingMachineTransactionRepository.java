package com.vendingmachine.cli.persistence;

import com.vendingmachine.cli.model.VendingMachineTransaction;
import org.springframework.data.jpa.repository.JpaRepository;

public interface VendingMachineTransactionRepository extends JpaRepository<VendingMachineTransaction, Long> {

}
