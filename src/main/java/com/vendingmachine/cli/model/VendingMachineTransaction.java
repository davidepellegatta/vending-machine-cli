package com.vendingmachine.cli.model;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name="TRANSACTIONS")
public class VendingMachineTransaction {


    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private Long itemId;

    private Date sellingDate;

    private Double price;


    public VendingMachineTransaction(Long itemId, Date sellingDate, Double price) {
        this.itemId = itemId;
        this.sellingDate = sellingDate;
        this.price = price;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getItemId() {
        return itemId;
    }

    public void setItemId(Long itemId) {
        this.itemId = itemId;
    }

    public Date getSellingDate() {
        return sellingDate;
    }

    public void setSellingDate(Date sellingDate) {
        this.sellingDate = sellingDate;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }
}
