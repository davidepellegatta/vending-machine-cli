package com.vendingmachine.cli.service;


/**
 * Manage the interaction with the cli
 */
public interface CliService {


    /**
     * Evaluates user input on command line
     *
     * @param userInteraction Text insert by vending machine user
     *
     * @return
     */
    String evaluateString(String userInteraction);


    /**
     * Return the welcome message
     * @return
     */
    String getWelcomeMessage();


    /**
     * Return the help message
     * @return
     */
    String getHelp();

    /**
     * Return a message when an invalid string is entered
     * @return
     */
    String invalidEntry();

}
