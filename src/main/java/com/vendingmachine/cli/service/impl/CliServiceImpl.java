package com.vendingmachine.cli.service.impl;

import com.vendingmachine.cli.model.FoodItem;
import com.vendingmachine.cli.model.VendingMachineTransaction;
import com.vendingmachine.cli.persistence.ItemRepository;
import com.vendingmachine.cli.persistence.VendingMachineTransactionRepository;
import com.vendingmachine.cli.service.CliService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.Optional;

@Service
public class CliServiceImpl implements CliService {

    private final static String HELP = "help";

    @Autowired
    private ItemRepository itemRepository;

    @Autowired
    private VendingMachineTransactionRepository transactionRepository;


    @Override
    public String evaluateString(String userInteraction) {

        switch (userInteraction){

            case HELP:

                return getHelp();


            default:
                return goToQuery(userInteraction);

        }
    }

    @Override
    public String getWelcomeMessage() {
        return "Welcome to the vending machine demo program.\n For Help write \"help<return>\"\n";
    }

    @Override
    public String getHelp() {
        return "Write the name of the product you want to buy.\n For the list of available products type \"products<return>\"\n";
    }

    @Override
    public String invalidEntry() {
        return "Sorry! I couldn't evaluate your request. \n " + getHelp();
    }


    private String goToQuery(String item){

        Optional<FoodItem> foodItemOptional = itemRepository.findByCliId(item);

        String returnMessage = "";

        if(foodItemOptional.isPresent()){

            //Update quantity
            FoodItem foodItem = foodItemOptional.get();
            foodItem.setQuantity(foodItem.getQuantity()-1);

            itemRepository.save(foodItem);

            //Create new Transaction
            transactionRepository.save(new VendingMachineTransaction(foodItem.getId(), new Date(), foodItem.getPrice()));

            returnMessage = foodItem.getName() + " delivered! /n";

        }else{

            //Manage product affinity
        }

        return returnMessage;
    }
}
