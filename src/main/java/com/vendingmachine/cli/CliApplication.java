package com.vendingmachine.cli;

import com.vendingmachine.cli.service.CliService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import java.util.Scanner;

@SpringBootApplication
@EnableJpaRepositories("com.vendingmachine.cli.persistence")
@ComponentScan(basePackages = "com.vendingmachine.cli")
public class CliApplication implements CommandLineRunner {

    @Autowired
    private CliService cliService;

	public static void main(String[] args) {

	    SpringApplication.run(CliApplication.class, args);
	}


    @Override
    public void run(String... args) throws Exception {

	    Scanner scanner = new Scanner(System.in);

        System.out.print(cliService.getWelcomeMessage());


        while(true){

            System.out.print(" >  ");

            System.out.println(cliService.evaluateString(scanner.next()));
        }
    }
}